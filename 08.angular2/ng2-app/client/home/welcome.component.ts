import { Component } from '@angular/core';
import template from './welcome.component.html'
@Component({
    template
})
export class WelcomeComponent {
    public pageTitle: string = "Welcome";
}
