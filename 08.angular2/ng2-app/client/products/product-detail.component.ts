import { Component } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import template from './product-detail.component.html'

@Component({
    template
})
export class ProductDetailComponent  {
    pageTitle: string = 'Product Detail';

    constructor(private route: ActivatedRoute, private router:Router) {
      this.route.params.subscribe((params) => {
        let id= params['id'];
        this.pageTitle += `: ${id}`;
      });

          // let id = +this._routeParams.get('id');
          // this.pageTitle += `: ${id}`;
    }

    onBack(): void {
        this.router.navigate(['products']);
    }

}
