
import {bootstrap} from "angular2-meteor-auto-bootstrap";
import {AppComponent} from "./app.component";
import {WelcomeComponent} from "./home/welcome.component";
import {ProductListComponent} from "./products/product-list.component";
import {ProductDetailComponent} from "./products/product-detail.component";
import {provideRouter, RouterConfig} from "@angular/router";
import {APP_BASE_HREF} from "@angular/common";
import {provide} from "@angular/core";

const routes: RouterConfig = [
  // { path: 'welcome', component: WelcomeComponent, name: 'Welcome' },
  // { path: 'products',	component: ProductListComponent, name: 'Products' },
  // { path: 'product/:id',	component: ProductDetailComponent, name: 'ProductDetail' },
  { path: '', component: WelcomeComponent },
  { path: 'welcome', component: WelcomeComponent },
  { path: 'products',	component: ProductListComponent },
  { path: 'product/:id',	component: ProductDetailComponent },
];

const APP_ROUTER_PROVIDERS = [

  provideRouter(routes)
];

bootstrap(AppComponent, [APP_ROUTER_PROVIDERS,provide(APP_BASE_HREF, { useValue: '/' })]);
